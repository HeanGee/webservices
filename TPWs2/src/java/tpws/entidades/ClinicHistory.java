/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpws.entidades;

import java.io.Serializable;

/**
 *
 * @author hean
 */
public class ClinicHistory implements Serializable
{
   private String fecha;
   private String id_paciente;
   private String nom_ape_paciente;
   private int edad;
   private String motivo_consulta_sintoma;
   private String enfermedad_diagnosticada;
   private String tratamiento_seguimiento;
   private String hospital;
   
   public ClinicHistory() {}

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getId_paciente() {
        return id_paciente;
    }

    public void setId_paciente(String id_paciente) {
        this.id_paciente = id_paciente;
    }

    public String getNom_ape_paciente() {
        return nom_ape_paciente;
    }

    public void setNom_ape_paciente(String nom_ape_paciente) {
        this.nom_ape_paciente = nom_ape_paciente;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getMotivo_consulta_sintoma() {
        return motivo_consulta_sintoma;
    }

    public void setMotivo_consulta_sintoma(String motivo_consulta_sintoma) {
        this.motivo_consulta_sintoma = motivo_consulta_sintoma;
    }

    public String getEnfermedad_diagnosticada() {
        return enfermedad_diagnosticada;
    }

    public void setEnfermedad_diagnosticada(String enfermedad_diagnosticada) {
        this.enfermedad_diagnosticada = enfermedad_diagnosticada;
    }

    public String getTratamiento_seguimiento() {
        return tratamiento_seguimiento;
    }

    public void setTratamiento_seguimiento(String tratamiento_seguimiento) {
        this.tratamiento_seguimiento = tratamiento_seguimiento;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }
   
   
}
