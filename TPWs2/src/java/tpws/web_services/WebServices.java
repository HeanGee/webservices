/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpws.web_services;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import tpws.database.Conection;
import tpws.entidades.ClinicHistory;

/**
 *
 * @author Hean G
 */
@WebService(serviceName = "WebServices")
public class WebServices
{
//    private Conection conexion = new Conection();

    /** Realiza un <code>SELECT</code> de la base de datos
     *
     * @param columnaName - <code>String</code> que representa el nombre de la columna.
     * @param valorColumna - <code>String</code> que representa el contenido de la celda correspondiente a la columna.
     * @param usuario - <code>String</code> que representa al usuario permitodo por la base de datos.
     * @param pass - <code>String</code> que representa la contraseña de acceso.
     * @return Lista de historial clínico.
     */
    
    @WebMethod(operationName = "selectHistorial")
    public List<ClinicHistory> selectHistorial(
            @WebParam(name = "columnaName") String columnaName,
            @WebParam(name = "valorColumna") String valorColumna,
            @WebParam(name = "usuario") String usuario,
            @WebParam(name = "pass") String pass)
    {
        /** -------------------- VERIFICAR CONEXION ------------------------. */
        Conection conexion = new Conection();
        System.out.println();
        System.out.println(usuario);
        System.out.println(pass);
        try {
            conexion.conectar("bdcentral", usuario, pass);
        }
        catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        catch (Exception ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
        /** -------------------- ACCEDER A LA BASE DE DATOS ------------------------. */
        ResultSet resultSet1, resultset2;
        List<ClinicHistory> listOfClinicHistory = new ArrayList<>();
        
        StringBuilder query = new StringBuilder();
        
        /** Construye sentencia SQL segun el valor de <code>valorColumna</code>. */
        if(valorColumna.equals(""))
            query.append("SELECT * FROM clinic_history");
        else
            query.append("SELECT * FROM clinic_history WHERE "+columnaName+" LIKE '%"+valorColumna+"%'");

        try {
            /** Realiza la consulta. */
            resultSet1 = conexion.select(query.toString());
            resultSet1.beforeFirst();
            resultSet1.next();
            
            while(!resultSet1.isAfterLast() && !resultSet1.isBeforeFirst()) {
                
                /** Si el resultset1 tiene filas, si o si hay cliente, por lo que
                 * resultset2 nunca puede ser nulo en este punto del código.
                 */
                resultset2 = conexion.select(
                    "SELECT * FROM pacientes WHERE cedula LIKE '%"+resultSet1.getString("id_paciente")+"%'");
                resultset2.next();
                
                ClinicHistory clinicHistory = new ClinicHistory();

                clinicHistory.setHospital(resultSet1.getString("hospital"));
                clinicHistory.setId_paciente(resultSet1.getString("id_paciente"));
                clinicHistory.setFecha(resultSet1.getString("fecha"));
                clinicHistory.setNom_ape_paciente(resultset2.getString("nombre"));
                clinicHistory.setEdad(Integer.parseInt(resultset2.getString("edad")));
                clinicHistory.setMotivo_consulta_sintoma(resultSet1.getString("motivo_consulta_sintoma"));
                clinicHistory.setEnfermedad_diagnosticada(resultSet1.getString("enfermedad_diagnosticada"));
                clinicHistory.setTratamiento_seguimiento(resultSet1.getString("tratamiento_seguir"));
                
                listOfClinicHistory.add(clinicHistory);
                
                resultSet1.next();
            }
            conexion.closeConection();
        }
        catch (SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        catch (Exception ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
        /** Retorna la lista. */
        return listOfClinicHistory;
    }
    
    /** Realiza un <>SELECT<> de la base de datos por rango de fecha.
     * <p>
     * <code>valorColumna1</code> representa al límite inferior y 
     * <code>valorColumna2</code> representa al límite superior.
     * </p>
     *
     * @param fecha
     * @param hospital
     * @param valorFecha1
     * @param valorFecha2
     * @param valorHospital
     * @param usuario
     * @param pass
     * @return Lista filtrada.
     */
    @WebMethod(operationName = "selectHistorialRango")
    public List<ClinicHistory> selectHistorialRango(
            @WebParam(name = "fecha") String fecha,
            @WebParam(name = "hospital") String hospital,
            @WebParam(name = "valorFecha1") String valorFecha1,
            @WebParam(name = "valorFecha2") String valorFecha2,
            @WebParam(name = "valorHospital") String valorHospital,
            @WebParam(name = "usuario") String usuario,
            @WebParam(name = "pass") String pass)
    {
        /** -------------------- VERIFICAR CONEXION  ------------------------. */
        Conection conexion = new Conection();
        System.out.println();
        System.out.println(usuario);
        System.out.println(pass);
        try {
            conexion.conectar("bdcentral", usuario, pass);
        }
        catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        catch (Exception ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
        /** -------------------- ACCEDER A LA BASE DE DATOS ------------------------. */
        ResultSet resultSet1, resultset2;
        List<ClinicHistory> listOfClinicHistory = new ArrayList<>();
        
        try {
            /** Realiza SELECT con Filtro. */
            resultSet1 = conexion.select(
                    "SELECT * FROM clinic_history "
                  + "WHERE "+fecha+">='"+valorFecha1+"' "
                  + "AND "+fecha+"<='"+valorFecha2+"' "
                  + "AND "+hospital+" LIKE '%"+valorHospital+"%'");
            resultSet1.beforeFirst();
            resultSet1.next();
            
            while(!resultSet1.isAfterLast() && !resultSet1.isBeforeFirst()) {
                
                /** Si el resultset1 tiene filas, si o si hay cliente, por lo que
                 * resultset2 nunca puede ser nulo en este punto del código.
                 */
                resultset2 = conexion.select(
                    "SELECT * FROM pacientes WHERE cedula LIKE '%"+resultSet1.getString("id_paciente")+"%'");
                resultset2.next();
                
                ClinicHistory clinicHistory = new ClinicHistory();
                
                clinicHistory.setHospital(resultSet1.getString("hospital"));
                clinicHistory.setId_paciente(resultSet1.getString("id_paciente"));
                clinicHistory.setFecha(resultSet1.getString("fecha"));
                clinicHistory.setNom_ape_paciente(resultset2.getString("nombre"));
                clinicHistory.setEdad(Integer.parseInt(resultset2.getString("edad")));
                clinicHistory.setMotivo_consulta_sintoma(resultSet1.getString("motivo_consulta_sintoma"));
                clinicHistory.setEnfermedad_diagnosticada(resultSet1.getString("enfermedad_diagnosticada"));
                clinicHistory.setTratamiento_seguimiento(resultSet1.getString("tratamiento_seguir"));
                
                listOfClinicHistory.add(clinicHistory);
                
                resultSet1.next();
            }
            conexion.closeConection();
        }
        catch (SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        catch (Exception ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

        /** Retorna la lista. */
        return listOfClinicHistory;
    }
    
    /** Realiza un <>INSERT<> mediante uso de List<T>.
     *
     * @param listOfClinicHistory
     * @param usuario
     * @param pass
     * @return Estado de la operacion.
     */
    @WebMethod(operationName = "insertarHistorial")
    public String insertarHistorial(
            @WebParam(name = "listOfClinicHistory") List<ClinicHistory> listOfClinicHistory,
            @WebParam(name = "usuario") String usuario,
            @WebParam(name = "pass") String pass)
    {
        /** ----------------- VERIFICAR CONEXION -----------------. */
        Conection conexion = new Conection();
        System.out.println();
        System.out.println(usuario);
        System.out.println(pass);
        StringBuilder msg = new StringBuilder();
        try {
            conexion.conectar("bdcentral", usuario, pass);
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            msg.append("No se encontro un controlador para base de dato.");
            return msg.toString();
        }
        catch (SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            msg.append("Error de SQL: ").append(ex.getLocalizedMessage());
            return msg.toString();
        }
        
        /** ----------------- ACCEDE A LA BASE DE DATOS -----------------. */
        ResultSet resSet;
        
        try {
            for(int i=0; i<listOfClinicHistory.size(); i++) {
                msg = msg.delete(0, msg.length());
                /** Verifica si el paciente ya existe en la central. */
                resSet = conexion.select(
                    "SELECT COUNT(*) FROM pacientes WHERE cedula LIKE '%"
                    +listOfClinicHistory.get(i).getId_paciente()+"%'"
                ); /** resSet tiene al menos 1 fila. */
                resSet.beforeFirst();
                resSet.next();
                if(resSet.getString(1).equals("0")) // Si el paciente es nuevo.
                {
                    /* Registrar paciente */
                    conexion.modify(
                        "INSERT INTO pacientes ("
                        + "cedula, nombre, edad) "
                        + "VALUES ('"
                        + listOfClinicHistory.get(i).getId_paciente()+"','"
                        + listOfClinicHistory.get(i).getNom_ape_paciente()+"','"
                        + listOfClinicHistory.get(i).getEdad()+"')"
                    );
                    /* Registrar historial. */
                    conexion.modify(
                        "INSERT INTO clinic_history ("
                        + "fecha, "
                        + "id_paciente, "
                        + "motivo_consulta_sintoma, "
                        + "enfermedad_diagnosticada, "
                        + "tratamiento_seguir, "
                        + "hospital"
                        + ") "
                        + "VALUES ('"
                        +(listOfClinicHistory.get(i)).getFecha().trim()+"','"
                        +(listOfClinicHistory.get(i)).getId_paciente().trim()+"','"
                        +(listOfClinicHistory.get(i)).getMotivo_consulta_sintoma().trim()+"','"
                        +(listOfClinicHistory.get(i)).getEnfermedad_diagnosticada().trim()+"','"
                        +(listOfClinicHistory.get(i)).getTratamiento_seguimiento().trim()+"','"
                        +(listOfClinicHistory.get(i)).getHospital().trim()+"')"
                    );
                    msg.append("Historial Clinico INSERTADO.");
                }
                else {
                    /** Si hay paciente, analiza si ya tiene tambien algun historial del hospital actual. */
                    resSet = conexion.select(
                            "SELECT COUNT(*) FROM clinic_history "
                            + "WHERE "
                            + "hospital LIKE '%"+listOfClinicHistory.get(i).getHospital()+"%' "
                            + "AND "
                            + "id_paciente LIKE '%"+listOfClinicHistory.get(i).getId_paciente()+"%'"
                    ); /** resSet tiene al menos 1 fila. */
                    resSet.beforeFirst();
                    resSet.next();
                    if(resSet.getString(1).equals("0")) { // Si el paciente no es del hospital actual.
                        conexion.modify(
                            "INSERT INTO clinic_history ("
                            + "fecha, "
                            + "id_paciente, "
                            + "motivo_consulta_sintoma, "
                            + "enfermedad_diagnosticada, "
                            + "tratamiento_seguir, "
                            + "hospital"
                            + ") "
                            + "VALUES ('"
                            +(listOfClinicHistory.get(i)).getFecha()+"','"
                            +(listOfClinicHistory.get(i)).getId_paciente()+"','"
                            +(listOfClinicHistory.get(i)).getMotivo_consulta_sintoma()+"','"
                            +(listOfClinicHistory.get(i)).getEnfermedad_diagnosticada()+"','"
                            +(listOfClinicHistory.get(i)).getTratamiento_seguimiento()+"','"
                            +(listOfClinicHistory.get(i)).getHospital()+"')"
                        );
                        msg.append("Historial Clinico INSERTADO.");
                    }
                    else {  // Si el paciente es del hospital actual.
                        conexion.modify(
                            "UPDATE clinic_history SET "
                            + "fecha='"+listOfClinicHistory.get(i).getFecha()+"', "
                            + "motivo_consulta_sintoma='"+listOfClinicHistory.get(i).getMotivo_consulta_sintoma()+"', "
                            + "enfermedad_diagnosticada='"+listOfClinicHistory.get(i).getEnfermedad_diagnosticada()+"', "
                            + "tratamiento_seguir='"+listOfClinicHistory.get(i).getTratamiento_seguimiento()+"' "
                            + "WHERE "
                            + "hospital='"+listOfClinicHistory.get(i).getHospital()+"' "
                            + "AND "
                            + "id_paciente='"+listOfClinicHistory.get(i).getId_paciente()+"'"
                        );
                        msg.append("Historial Clinico ACTUALIZADO.");
                    }
                }
            }
            conexion.closeConection();
        }
        catch (SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            msg.append("Error de SQL: ").append(ex.getLocalizedMessage());
            return msg.toString();
        }
        
        return msg.toString();
    }
}
