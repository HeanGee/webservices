/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpws.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hean
 */
public class Conection
{
    private Connection conector;
    private boolean conectado;
    
    public final static int SUCCESFULL_MODIFICATION = 1;
    public final static int SUCCESFULL_NON_MODIFICATION = 0;
    public final static int SQL_EXCEPTION = -1;
    public final static int NOT_CONNECTED = -2;
    
    /** Constructor. */
    public Conection() {
        this.conectado = false;
    }
    
    /** Conectar con la base de datos.
     * 
     * @param databaseName
     * @param user
     * @param pass
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    public void conectar(String databaseName, String user, String pass) throws ClassNotFoundException, SQLException
    {
        if(!this.getConectado()) {
            Class.forName("com.mysql.jdbc.Driver");
            this.conector = DriverManager.getConnection("jdbc:mysql://localhost/"+databaseName, user, pass);
            this.setConectado(true);
        }
    }
    
    /** Método que abstrae la implementación de SELECT.
     * 
     * @param sqlQuery String que representa la sintaxis de consulta SQL.
     * @return Resultado de consulta SQL. Se puede navegar sobre dicho objeto.
     */
    
    public ResultSet select(String sqlQuery) throws SQLException
    {
        if(this.getConectado())
        {
            System.out.println(sqlQuery);
            Statement consultor = this.conector.createStatement();
            ResultSet resultadoSQL = consultor.executeQuery(sqlQuery);
            return resultadoSQL;
        }
        return null;
    }
    
    /** Método que abstrae la implementación de INSERT, DELETE, UPDATE.
     *
     * @param sqlQuery Query de tipo: Update, Delete, Insert.
     * @return 
     * @throws java.sql.SQLException
     */
    public String modify(String sqlQuery) throws SQLException
    {
        String SQLOperation = sqlQuery.substring(0, sqlQuery.indexOf(" ")+1);
        StringBuilder msg = new StringBuilder(SQLOperation+" NO efectuada");
        if(this.getConectado())
        {
            System.out.println(sqlQuery);
            Statement consultor = this.conector.createStatement();
            consultor.executeUpdate(sqlQuery);
            msg = msg.delete(0, msg.length());
            msg.append(SQLOperation).append(" efectuada.");
        }
        return msg.toString();
    }
    
    /** Cerrar conexion con la base de datos. 
     * 
     * @throws java.sql.SQLException Error cerrando conexión con la base de datos.
     */
    public void closeConection() throws SQLException
    {
        if(!this.conector.isClosed()) {
            this.conector.close();
            this.setConectado(false);
        }
    }
    
    public void setConectado(boolean flag) {
        this.conectado = flag;
    }
    
    public boolean getConectado() {
        return this.conectado;
    }
}
