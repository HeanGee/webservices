/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.12-log : Database - bdcentral
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bdcentral` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `bdcentral`;

/*Table structure for table `clinic_history` */

DROP TABLE IF EXISTS `clinic_history`;

CREATE TABLE `clinic_history` (
  `hospital` varchar(45) NOT NULL,
  `id_paciente` varchar(45) NOT NULL,
  `fecha` datetime DEFAULT NULL,
  `motivo_consulta_sintoma` varchar(75) DEFAULT NULL,
  `enfermedad_diagnosticada` varchar(75) DEFAULT NULL,
  `tratamiento_seguir` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`hospital`,`id_paciente`),
  KEY `id_paciente` (`id_paciente`),
  CONSTRAINT `clinic_history_ibfk_1` FOREIGN KEY (`id_paciente`) REFERENCES `pacientes` (`cedula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pacientes` */

DROP TABLE IF EXISTS `pacientes`;

CREATE TABLE `pacientes` (
  `cedula` varchar(45) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  PRIMARY KEY (`cedula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
