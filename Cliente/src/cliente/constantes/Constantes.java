/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente.constantes;

/**
 *
 * @author Hean G
 */
public class Constantes {
    public static String[] IDENTIFICADORES = {
        "1", "2", "3", "4"
    };
    
    public static String hoyEs() {
        /** Obtiene la fecha con hora actual. */
        String hoy = new java.util.Date().toLocaleString();
        
        /** Competente solo para windows. */
        String fechaSinHora = (hoy.substring(0, 10)).replace('/', '-');
        
        String[] dia_mes_anho;
        dia_mes_anho = fechaSinHora.split("-");
        
        StringBuilder hoyNuevo = new StringBuilder();
        hoyNuevo
                .append(dia_mes_anho[2]).append("-")
                .append(dia_mes_anho[1]).append("-")
                .append(dia_mes_anho[0])
                .append(hoy.substring(10, 19));
        
        System.out.println(hoyNuevo.toString());
        
        return hoyNuevo.toString();
    }
    
    public static String[] NOMBRES = {
        "DANIEL MIN", "ESTER ALFONZO", "FERNANDO DELVALLE", "OSMAR OZUNA", "SAID MOSTAFA"
    };
    
    public static int[] EDADES = {
        15, 20, 25, 30, 35, 40, 45
    };
    
    public static String[] MOTIVO_CONSULTA = {
        "Tos Frecuente", "Comezón insaciable", "Fiebre Nocturno", "Mareos diurnos",
        "Cansancio insoportable", "Insomnio", "Nerviosismo incontrolable",
        "Furia", "... Y otros Posibles sintomas anormales ...", "Dolor de muela", 
        "Venas oculares visibles", "Molestias al beber gaseosa", 
        "No sabe qué tiene pero viene...", 
    };
    
    public static String[] ENFERMEDADES = {
        "Gripe Aviar", "SIDA", "Dengue", "Ebola",
        "Fiebre Amarilla", "Rabia", "Neumonia", "Pulmonia",
        "Hepatitis", "Indigestion", "Sobredosis de alcohol", "Diabetes", 
        "Hipertension", "Sarampion", 
    };
    
    public static String[] TRATAMIENTOS_A_SEGUIR = {
        "No comer mucho", "Dormir mas", "Hacer ejercicio", "Dormir menos",
        "Estudiar mas", "Mirar menos television", "Salir a caminar", "Evitar sedentarismo", 
        "Dejar de copiar", "Tratar de hacer solo", "Comer dos veces al dia",
        "No dormir", "Dormir mas", "... Y otras tonterias ...", 
    };
    
    public static String[] HOSPITALES = {
        "hospitalA", "", "hospitalB", "hospitalC", "hospitalD", "hospitalE",
        "hospitalF", "hospitalG", "hospitalH", "hospitalI", "hospitalJ",
        "hospitalK", "hospitalL", 
    };
    
    
    
}
